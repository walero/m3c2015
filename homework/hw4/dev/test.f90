program test
	use fdmodule2d
	implicit none
	real(kind=8) :: error(2), time
	real(kind=8) :: error_omp(2), time_omp
	!real(kind=8) :: wtf(2,3)

	open(unit=10,file='data.in')
        read(10,*) n1
        read(10,*) n2
        read(10,*) numthreads
    close(10)

    call test_grad(error,time)
    call test_grad_omp(error_omp,time_omp)

	!print *, 'error=',error
    !print *, 'time=',time
    print *, 'error omp=',error_omp
    print *, 'time omp=',time_omp
    !print *, 'wtf=',wtf(:,3)
end program test