!module containing routines for differentiating array of size N with
!2nd order and 4th order compact finite differences
!Test function applies these methods to a Gaussian function and 
!returns the error.

module fdmodule2d
    use omp_lib !makes OpenMP routines, variables available!
    use fdmodule
    implicit none
    integer :: n1,n2,numthreads
    real(kind=8) :: dx1,dx2
    save
contains
!-------------------
subroutine grad(f,df1,df2,df_amp,df_max)
    implicit none
    integer :: i1,i2
    real(kind=8), dimension(:,:), intent(in) :: f
    real(kind=8), dimension(size(f,1),size(f,2)), intent(out) :: df1,df2,df_amp
    real(kind=8), intent(out) :: df_max

    !allocate(f(n2,n1))
    N=n1
    dx=dx1
    do i1=1,n2
    !print *, i1
        call cfd4(f(i1,:),df1(i1,:))
    end do
    
    N=n2
    dx=dx2
    do i2=1,n1
    !print *, 'this is=',i2
        call cfd4(f(:,i2),df2(:,i2))
    end do

    df_amp=abs(df1)+abs(df2)
    df_max=maxval(abs(df1)+abs(df2))


end subroutine grad
!---------------------------
subroutine test_grad(error,time)
    !tests accuracy and speed of grad, assumes n1,n2,dx1,dx2 have been set in calling program
    implicit none
    integer :: i1,j1
    real(kind=8), allocatable, dimension(:) :: x1,x2 !coordinates stored in arrays
    real(kind=8), allocatable, dimension(:,:) :: x11,x22 !coordinates stored in matrices
    real(kind=8), allocatable, dimension(:,:) :: ftest,df1,df2,df_amp,df_amp_exact,df1_ftest,df2_ftest !test function and results frorom grad
    real(kind=8) :: df_max
    real(kind=8), intent(out) :: time,error(2) !error is two-element array
    integer(kind=8) :: tick1,tick2,rate

    allocate(x1(n1),x2(n2),x11(n2,n1),x22(n2,n1),ftest(n2,n1),df1(n2,n1),df2(n2,n1), &
        df_amp(n2,n1),df_amp_exact(n2,n1),df1_ftest(n2,n1),df2_ftest(n2,n1))

    !generate mesh
    dx1 = 1.d0/dble(n1-1)
    dx2 = 1.d0/dble(n2-1)

    do i1=1,n1
        x1(i1) = dble(i1-1)*dx1
    end do

    do i1=1,n2
        x2(i1) = dble(i1-1)*dx2
    end do

    do i1=1,n2
        x11(i1,:) = x1
    end do

    do i1=1,n1
        x22(:,i1) = x2
    end do

    ftest = sin(x11)*cos(x22)
    df1_ftest = cos(x11)*cos(x22) !test function
    df2_ftest = -sin(x11)*sin(x22)

    call system_clock(tick1)
    call grad(ftest,df1,df2,df_amp,df_max)
    call system_clock(tick2,rate)
    time=dble(tick2-tick1)/dble(rate)
    error(1)=(1.d0/n2/n1)*sum(abs(df1_ftest-df1))
    error(2)=(1.d0/n1/n2)*sum(abs(df2_ftest-df2))

end subroutine test_grad
!-------------------
subroutine grad_omp(f,df1,df2,df_amp,df_max)
    implicit none
    logical, parameter :: debug = .false.
    integer :: i1,i2,w,j
    real(kind=8), dimension(:,:), intent(in) :: f
    real(kind=8), dimension(size(f,1),size(f,2)), intent(out) :: df1,df2,df_amp
    real(kind=8), intent(out) :: df_max

    N=n1
    dx=dx1
!$omp parallel do
    do i1=1,n2 
        call cfd4(f(i1,:),df1(i1,:))
    end do
!$omp end parallel do
    N=n2
    dx=dx2
!$omp parallel do
    do i2=1,n1
        call cfd4(f(:,i2),df2(:,i2))
    end do
!$omp end parallel do

    if (debug) then
!$omp parallel
  NumThreads = omp_get_num_threads()
    print *, 'NumThreads=',NumThreads
!$omp end parallel
    end if

!$omp parallel do private(w,j) reduction(max:df_max)
    do j=1,size(f,2)
        do w=1,size(f,1)
        df_amp(w,j)=abs(df1(w,j))+abs(df2(w,j))
        df_max=max(df_max, abs(df1(w,j))+abs(df2(w,j)))
        end do
    end do
!$omp end parallel do
!print *,'max=',df_max


end subroutine grad_omp
!---------------------------
subroutine test_grad_omp(error,time)
    !tests accuracy and speed of grad_omp, assumes module variables n1,n2,dx1,dx2,numthreads have been set in calling program
    implicit none
    integer :: i1,j1
    real(kind=8), allocatable, dimension(:) :: x1,x2 !coordinates stored in arrays
    real(kind=8), allocatable, dimension(:,:) :: x11,x22 !coordinates stored in matrices
    real(kind=8), allocatable, dimension(:,:) :: ftest,df1,df2,df_amp,df_amp_exact,df1_ftest,df2_ftest !test function and results from grad
    real(kind=8) :: df_max
!    integer(kind=8) :: t1,t2,clock_rate
    real(kind=8), intent(out) :: time,error(2) !error is two-element array
    integer(kind=8) :: tock1,tock2,rate

    allocate(x1(n1),x2(n2),x11(n2,n1),x22(n2,n1),ftest(n2,n1),df1(n2,n1),df2(n2,n1), &
        df_amp(n2,n1),df_amp_exact(n2,n1))

    !generate mesh
    dx1 = 1.d0/dble(n1-1)
    dx2 = 1.d0/dble(n2-1)

    do i1=1,n1
        x1(i1) = dble(i1-1)*dx1
    end do

    do i1=1,n2
        x2(i1) = dble(i1-1)*dx2
    end do

    do i1=1,n2
        x11(i1,:) = x1
    end do

    do i1=1,n1
        x22(:,i1) = x2
    end do

    ftest = sin(x11)*cos(x22) !test function
    df1_ftest = cos(x11)*cos(x22) !test function
    df2_ftest = -sin(x11)*sin(x22)

!$  call omp_set_num_threads(numthreads)
    call system_clock(tock1)
    call grad_omp(ftest,df1,df2,df_amp,df_max)

    call system_clock(tock2,rate)
    time=dble(tock2-tock1)/dble(rate)
    error(1)=(1.d0/n2/n1)*sum(abs(df1_ftest-df1))
    error(2)=(1.d0/n1/n2)*sum(abs(df2_ftest-df2))

end subroutine test_grad_omp
!---------------------------
end module fdmodule2d













