!module containing routines for differentiating array of size N with
!2nd order and 4th order compact finite differences
!Test function applies these methods to a Gaussian function and 
!returns the error.

module fdmodule
    !use omp_lib
    implicit none
    integer :: N
    real(kind=8) :: dx
    save

contains
!-------------------
subroutine fd2(f,df)
    !2nd-order centered finite difference scheme with periodic boundary conditions
    implicit none
    real(kind=8) :: invtwodx
    real(kind=8), dimension(:), intent(in) :: f
    real(kind=8), dimension(size(f)), intent(out) :: df

    invtwodx = 0.5d0/dx
    df(2:N-1) = invtwodx*(f(3:N)-f(1:N-2))

    df(1) = invtwodx*(f(2) - f(N))
    df(N) = invtwodx*(f(1) - f(N-1))

end subroutine fd2
!-----------------

subroutine cfd4(f,df)
    !4th order compact finite difference
    implicit none
    real(kind=8) :: dxfac,c1,c2,c3,c4
    real(kind=8), dimension(:), intent(in) :: f
    real(kind=8), dimension(size(f)), intent(out) :: df
!variables for DGTSV
    real(kind=8), dimension(size(f)) :: D
    real(kind=8), dimension(size(f)-1) :: DL,DU
    integer :: INFO

    dxfac = 1.d0/dx
    c1 = 3.d0
    c2 = 2.5d0
    c3 = 2.d0
    c4 = 0.5d0

    df(2:N-1) = c1*(f(3:N)-f(1:N-2))
    df(1) = (-c2*f(1) + c3*f(2) + c4*f(3))
    df(N) = (c2*f(N) - c3*f(N-1) - c4*f(N-2))

    df = dxfac*df


    D =  4.d0
    DL = 1.d0
    DU = 1.d0

    D(1) = 1.d0
    DU(1) = 2.d0
    D(N) = 1.d0
    DL(N-1) = 2.d0

    call DGTSV(N,1,DL,D,DU,df,N,INFO)


    if (info .ne. 0) then
        print *, 'error in compactfd4, DGTSV gives INFO=',INFO
        STOP
    end if

end subroutine cfd4
!------------------


subroutine test_fd(alpha,error)
    !test fd2 and cfd4 with Gaussian function
    implicit none
    integer :: i1
    real(kind=8), intent(in) :: alpha
    real(kind=8), intent(out) :: error(2)
    real(kind=8), dimension(N) :: x,fgauss,dfgauss,dfgauss_exact
    real(kind=8) :: x0

    !generate grid
    do i1=0,N-1
        x(i1+1) = dble(i1)*dx
    end do

    x0 = 0.5*x(N)

    fgauss = exp(-alpha*(x-x0)**2)
    dfgauss_exact = (-2.d0*alpha)*(x-x0)*fgauss

    call fd2(fgauss,dfgauss)
    error(1) = sum(abs(dfgauss-dfgauss_exact))/dble(N)

    call cfd4(fgauss,dfgauss)
    error(2) = sum(abs(dfgauss-dfgauss_exact))/dble(N)


end subroutine test_fd
!---------------------
!---------------------------
end module fdmodule















