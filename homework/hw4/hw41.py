""" Wale Osikomaiya CID: 00734031
generated first hw4.so file by 
f2py -llapack -c fdmodule_dev.f90 hw41_dev.f90 -m hw4 command in terminal

Assumes fdmodule
f2py -llapack --f90flags='-fopenmp' -lgomp -c fdmodule.f90 hw41.f90 -m hw4
"""
from hw4 import fdmodule as f1
from hw4 import fdmodule2d as f2d
import numpy as np
import matplotlib.pyplot as plt

def test_grad1(n1,n2,numthreads):
    """obtain errors and timing values from test_grad and test_grad_omp
    return the two errors and average times from ten calls to the test routines
    Input: n1: number of points in x1
           n2: number of points in x2
           numthreads: number of threads used in test_grad_omp
    """
    f2d.n1=n1
    f2d.n2=n2
    f2d.numthreads=numthreads
    time=np.zeros(10)
    time_omp=np.zeros(10)
    for i in range(0,10):
        time[i]=f2d.test_grad()[1]
        time_omp[i]=f2d.test_grad_omp()[1]
    t=np.mean(time)
    tp=np.mean(time_omp)
    e=f2d.test_grad()[0]
    ep=f2d.test_grad_omp()[0]

    return e,ep,t,tp
     
def test_gradN(numthreads):
    """input: number of threads used in test_grad_omp
    description of the trend:
    figure(1) shows that as the grid size (n1xn2) increases the speedup increases
        though plateaus after a point for example, when numthreads=2 after grid
        size 125000, the graph begins to plateau around 1.95 and has a supremum
        of around 2
        Moreover, the figure show a little difference between the square matric and 
        rectangular matrix, with the rectangular reaching maximum speedup quicker but  
        this still implies the only real obvious trend is between the size of
        the grids (n1xn2) and rather than the shape

    figure(2) shows that the computational increases as the grid size (n1xn2)
        as expected, though the important trend can be seen in figure(3) where
        the rate of increase for computational as grid size is clearly smaller
        for the parallel code by a constant value
    """
    N=np.arange(100,650,50)
    N_rec=(N**2)/100
    tN=np.zeros((2,len(N)))
    t=np.zeros((2,len(N)))
    tp=np.zeros((2,len(N)))
    f2d.numthreads=numthreads
    for k in range(0,2):
        for i in range(0,len(N)):
            if k == 0:
                f2d.n1=N_rec[i]
                f2d.n2=N[1]
            else:
                f2d.n1=N[i]
                f2d.n2=N[i]
            time=np.zeros(10)
            time_omp=np.zeros(10)
            for j in range(0,10):
                time[j]=f2d.test_grad()[1]
                time_omp[j]=f2d.test_grad_omp()[1]
            t[k,i]=np.mean(time)
            tp[k,i]=np.mean(time_omp)
            tN[k,i]=t[k,i]/tp[k,i]
    plt.figure(figsize=(14, 8))
    plt.xlabel('grid size N=' r'$n_{1}\times n_{2}$')
    plt.ylabel('speedup =' r'$\frac{Ts}{Tp}$')
    plt.title('test_gradN, Wale Osikomaiya, grid size against speedup, numthreads=%d' %(numthreads))
    plt.plot(N**2,tN[0,:],label='rectangular shape matrix')
    plt.plot(N**2,tN[1,:],label='square shape  matrix')
    plt.legend(loc='best')
    plt.savefig("hw41_1.png", dpi=800)

    plt.figure(figsize=(14, 8))
    plt.xlabel('grid size N=' r'$n_{1}\times n_{2}$')
    plt.ylabel('computational time')
    plt.title('test_gradN, Wale Osikomaiya, grid size against computational time, square shape matrix, numthreads=%d' %(numthreads))
    plt.plot(N**2,t[1,:],label='serial $Ts$')
    plt.plot(N**2,tp[1,:],label='parallel $Tp$')
    plt.legend(loc='best')
    plt.savefig("hw41_2.png", dpi=800)

    plt.figure(figsize=(14, 8))
    plt.xlabel('grid size N=' r'$n_{1}\times n_{2}$')
    plt.ylabel('computational time')
    plt.title('test_gradN, Wale Osikomaiya, log plot of grid size against computational time, square shape matrix, numthreads=%d' %(numthreads))
    plt.loglog(N**2,t[1,:],label='serial $Ts$')
    plt.loglog(N**2,tp[1,:],label='parallel $Tp$')
    plt.legend(loc='best')
    plt.savefig("hw41_3.png", dpi=800)
    
    return tN



#This section will be used for assessment, you may alter the call to test_gradN if desired, but
#the other lines should be left as is in your final submission.    
if __name__ == "__main__":
    
    e,ep,t,tp = test_grad1(400,200,2)
    tN = test_gradN(2) #modify if anything is being returned
    plt.show()
