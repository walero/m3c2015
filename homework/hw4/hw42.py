"""Solve advection equation with odeint and fortran fd2 routine in fdmodule
generated fd.so file by 
f2py  -llapack --f90flags='-fopenmp' -lgomp -c fdmodule.f90 -m fd

"""    
    
from fd import fdmodule as f1 #you may change this line if desired
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

def advection1(tf,n,dx,c=1.0,S=0.0,display=False):
    """solve advection eqn, df/dt + c df/dx = S 
    where c and S are constants, x=0,dx,...(N-1)*dx,
    and f is periodic, f(x_i) = f(x_i+N*dx).
    Initial condition: f(x,t=0) = exp(-100.0*(x-x0)**2)
    Returns: f(x,tf)
    """

    x=np.zeros(n)
    for i in range(0,n):
        x[i]=i*dx
    t=np.linspace(0,tf,n)
    x0=((n-1)*dx)/2
    f0=np.exp(-100*(x-x0)**2)
    #function provides RHS to odeint
    def RHS(f,t,dx,n,c,S):
        f1.n=n
        f1.dx=dx
        return S-(c*f1.fd2(f))
    F=odeint(RHS,f0,t,args=(dx,n,c,S),mxstep=50000)
    if display == True: 
        plt.figure(figsize=(14, 8))
        plt.xlabel('$x$')
        plt.ylabel('$f(x,tf)$')
        plt.title('advection1, Wale Osikomaiya, computed solution of linear advection equation, $tf=$%2.1f, $n=$%d, $c=$%2.1f, $S=$%2.1f ' %(tf,n,c,S))
        plt.plot(x,F[n-1],label='computed solution')
        plt.legend(loc='best')
        plt.savefig("hw42_1.png", dpi=800)
    #print len(F[n-1])
    return F[n-1]
  
def test_advection1():
    n=np.arange(100,4000+1,100)
    error=np.zeros(len(n))
    for i in range(0,len(n)):
        dx=1./n[i]
        x=np.zeros(n[i])
        for k in range(0,n[i]):
            x[k]=k*dx
        tf=1
        c=1
        S=1
        F=advection1(tf,n[i],dx,c,S,display=False)
        #t=np.linspace(0,tf,n)
        x0=((n[i]-1)*dx)/2
        F_exact=(S*tf)+np.exp(-100.0*(x-x0)**2)
        error[i]=(1./n[i])*np.sum(np.abs(F-F_exact))
        if len(n[0:i+1]) > 1:
            z=np.polyfit(np.log(n[0:i+1]),np.log(error[0:i+1]),1)
            assert (2*(1+0.05)>= abs(z[0]) >= 2*(1-0.05)),"Error, computed convergence rate not within 5'%'of the analytical "
            #print abs(z[0])
            #if 2*(1+0.05)>= abs(z[0]) >= 2*(1-0.05):
            #    j=i
            #    break
    #n_value=n[j]
    plt.figure(figsize=(14, 8))
    plt.xlabel('$n$')
    plt.ylabel('$error$')
    plt.title('test_advection1, Wale Osikomaiya, error of computed solution of linear advection equation against n, $tf=$%2.1f, $c=$%2.1f, $S=$%2.1f ' %(tf,c,S))
    plt.loglog(n,error,label='error of computed solution')
    plt.legend(loc='best')
    plt.savefig("hw42_2.png", dpi=800)
    m=np.polyfit(np.log(n),np.log(error),1)
    return abs(m[0])
                                                                               
#This section will be used for assessment, you may enter a call to fdtest_eff, but
#the three un-commented lines should be left as is in your final submission.    
if __name__ == "__main__":
    n = 1000
    dx = 1.0/float(n-1)
    f = advection1(2,n,dx,1,1,True)
    m = test_advection1()
    plt.show()
