"""Wale Osikomaiya
00734031
"""

import numpy as np
import matplotlib.pyplot as plt
import numpy.random as npr


#---------------------
def test_randn(n,m=0,s=1):
    """Input variables:
    n   number of normal random variables to be generated
    m   random variables generated with mean = m
    s   random variables ge§nerated with standard deviation = s
    """
    N=npr.randn(n)
    plt.figure()
    plt.xlabel('value')
    plt.ylabel('frequency')
    plt.title('test_randn, Wale Osikomaiya, normally distrubted random numbers with n=%d, m=%2.1f, s=%2.1f'%(n,m,s))
    plt.hist(m+(N*s))     
 
#---------------------
def wiener1(dt,X0,Nt):
    """ Input variables:
    dt    time step
    X0    intial value, X(t=0) = X0
    Nt    number of time steps 
    """
    Xdt=np.sqrt(dt)*npr.randn(Nt+1)
    Xdt[0]=X0
    w=np.cumsum(Xdt)
    t=np.linspace(0,dt*Nt,Nt+1)
    sqrt=np.sqrt(t)
    msqrt=(-1)*np.sqrt(t)
    plt.figure()
    plt.xlabel('t')
    plt.ylabel('X(t)')
    plt.title('wiener1, Wale Osikomaiya, Weiner process with $dt$=%2.3f, $X_{0}$=%d, $Nt$=%d ' %(dt, X0, Nt))
    plt.plot(t,w,'b-') #blue line! 
    plt.plot(t,sqrt,'r--') #red dashed line!
    plt.plot(t,msqrt,'r--') #red dashed line!

    
    
#---------------------
def wienerM(dt,X0,Nt,M):
    """ Input variables:
    dt    time step
    X0    intial value, X(t=0) = X0
    Nt    number of time steps 
    M     number of samples
    """
    Xdt=np.sqrt(dt)*npr.randn(M,Nt+1)
    Xdt[:,0]=X0
    w=np.cumsum(Xdt,axis=1)
    me=np.mean(w,axis=0)
    std=np.std(w,axis=0)
    t=np.linspace(0,dt*Nt,Nt+1)
    mu=np.linspace(X0,X0,Nt+1)
    sqrt=np.sqrt(t)
    msqrt=(-1)*np.sqrt(t)
    plt.figure()
    plt.xlabel('t')
    plt.ylabel('X(t)')
    plt.title('wienerM, Wale Osikomaiya, Monte Carlo simulation of Weiner process with $M$=%d , $dt$=%2.3f , $X_{0}$=%d , $Nt$=%d' %(M, dt, X0, Nt))
    plt.plot(t,me,'b-', label='computed $\mu$') #blue line!
    plt.plot(t,mu,'b--', label=r'$\mu = X_{0}$') #blue dashed line!
    plt.plot(t,std,'g-', label='computed standard $\sigma$') #green line!
    plt.plot(t,sqrt,'r--', label=r'$\sigma = \sqrt{t}$') #red dashed line!
    plt.plot(t,msqrt,'r--') #red dashed line!
    plt.legend(loc='best')


#-----------------------------
def langevin1(dt,V0,Nt,g=1.0):
    """ Input variables:
    dt    time step
    V0    intial value, V(t=0) = V0
    Nt    number of time steps 
    g     friction factor, gamma
    """
    Vdt=np.zeros(Nt+1)
    Vdt[0]=V0
    for i in range(1, Nt+1):
        Vdt[i]=Vdt[i-1]-(g*Vdt[i-1]*dt)+(np.sqrt(dt)*npr.randn())
    t=np.linspace(0,dt*Nt,Nt+1)
    plt.figure()
    plt.xlabel('t')
    plt.ylabel('V(t)')
    plt.title('langevin1, Wale Osikomaiya, Langevin version of Weiner process with $dt$=%2.3f, $V_{0}$=%d, $Nt$=%d, g=%d ' %(dt, V0, Nt, g))
    plt.plot(t,Vdt,'b-') #blue line!
    
#-----------------------------
def langevinM(dt,V0,Nt,M,g=1.0,debug=False):
    """ Input variables:
    dt    time step
    V0    intial value, V(t=0) = V0
    Nt    number of time steps 
    M     number of samples
    g     friction factor, gamma
    debug flag to control plotting of figures
    """
    Vdt=np.zeros((M,Nt+1))
    dummy=np.zeros((M,Nt+1))
    Vdt[:,0]=V0
    for i in range(1, Nt+1):
        dummy[:,i]=npr.randn(1,M)
        Vdt[:,i]=Vdt[:,i-1]-(g*Vdt[:,i-1]*dt)+(np.sqrt(dt)*dummy[:,i])

    t=np.linspace(0,dt*Nt,Nt+1)
    meanVdt=np.mean(Vdt,axis=0)
    varVdt=(np.std(Vdt,axis=0))**2
    mean_ana=np.zeros(Nt+1)
    for  i in range(0, Nt+1):
        mean_ana[i]=V0*np.exp(-g*t[i])

    var_ana=np.zeros(Nt+1)
    for  i in range(0, Nt+1):
        var_ana[i]=(1./float((2*g)))*(1-np.exp(-2*g*t[i]))
    epsi=np.abs(meanVdt-mean_ana)
    epsil=((1./float(Nt))*np.cumsum(epsi))
    epsilon=epsil[Nt]

    if debug == True:
        plt.figure()
        plt.xlabel('t')
        plt.ylabel('V(t)')
        plt.title('langevinM, Wale Osikomaiya, Langevin version of Monte Carlo simulation with $M$=%d, $dt$=%2.3f, $V_{0}$=%d, $Nt$=%d, g=%d ' %(M, dt, V0, Nt, g))
        plt.plot(t,meanVdt,'b-', label='computed $\mu$') #blue line!
        plt.plot(t,mean_ana,'b--', label=r'$\mu = V_{0}e^{-\gamma t}$') #blue dashed line!
        plt.plot(t,varVdt,'g-', label='computed variance $\sigma^{2}$') #green line!
        plt.plot(t,var_ana,'g--', label=r'$\sigma^{2} =\frac{1}{2\gamma}(1-e^{-2\gamma t})$') #red dashed line!
        plt.legend(loc='best')
        return epsilon
    else:
        return epsilon

#--------------------------------
def langevinTest(dt,V0,Nt,g=1.0):
    """ Input variables:
    dt    time step
    V0    intial value, V(t=0) = V0
    Nt    number of time steps 
    g     friction factor, gamma
    """
    M=10000
    error=np.zeros(4)
    error[0]=langevinM(dt, V0, Nt, M/1000, g, False)
    error[1]=langevinM(dt, V0, Nt, M/100, g, False)
    error[2]=langevinM(dt, V0, Nt, M/10, g, False)
    error[3]=langevinM(dt, V0, Nt, M/1, g, False)
    plot_M=[10, 100, 1000, 10000]
    plt.figure()
    plt.loglog(plot_M,error)
    y=np.log(error)
    x=np.log(plot_M)
    z=np.polyfit(x,y,1)
    #np.poly1d(z)
    return z[0]
#---------------------------------
#The section below is included for assessment and *must* be included exactly as below in the final
#file that you submit, but you may want to omit it (e.g. comment it out) while you are developing
#your code.  
if __name__ == "__main__":
    npr.seed(1)
    test_randn(1000)
    wiener1(0.1,0.0,1000)
    wienerM(0.1,2.0,1000,20000)
    langevin1(0.1,0.0,1000,0.01)
    print "Error from LangevinM, dt=%1.2f,V0=%1.2f,Nt=%d,M=%d,g=%1.3f: "%(0.1,1.0,1000,2000,0.05), langevinM(0.1,1.0,1000,2000,0.05,True)
    print "Convergence rate for Langevin Monte Carlo calculations from langevinTest", langevinTest(0.1,1.0,1000,0.25)
    plt.show()