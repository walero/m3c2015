"""Wale Osikomaiya
00734031
"""

import numpy as np
import matplotlib.pyplot as plt
import numpy.random as npr


#---------------------
def test_randn(n,m=0,s=1):
    """Input variables:
    n   number of normal random variables to be generated
    m   random variables generated with mean = m
    s   random variables generated with standard deviation = s
    """
    N=npr.randn(n) #creats an array of n random numbers
    plt.figure() 
    plt.xlabel('random number')
    plt.ylabel('frequency')
    plt.title('test_randn, Wale Osikomaiya, normally distrubted random numbers with n=%d, m=%2.1f, s=%2.1f'%(n,m,s))
    plt.hist(m+(N*s)) #plots a histogram of normally distrubted random numbers
 
#---------------------
def wiener1(dt,X0,Nt):
    """ Input variables:
    dt    time step
    X0    intial value, X(t=0) = X0
    Nt    number of time steps 
    """
    Xdt=np.sqrt(dt)*npr.randn(Nt+1) #creates an array of Nt+1 random numbers multipled by square root of dt which will be our X values
    Xdt[0]=X0 #makes the first entry of our Xdt array the starting value of X, X0
    w=np.cumsum(Xdt) #cumulatively sums each entry of our Xdt array, which represents the weiner procresss with timestep, dt, and initial condition X0
    t=np.linspace(0,dt*Nt,Nt+1) #creates a array for the values of time, with timestep dt
    sqrt=np.sqrt(t) #standard deviation of exact solution
    msqrt=(-1)*np.sqrt(t) #standard deviation of exact solution
    plt.figure()
    plt.xlabel('t')
    plt.ylabel('X(t)')
    plt.title('wiener1, Wale Osikomaiya, Weiner process with $dt$=%2.3f, $X_{0}$=%d, $Nt$=%d ' %(dt, X0, Nt))
    plt.plot(t,w,'b-', label='computed $X(t)$') #plots values of X(t) against t, blue line
    plt.plot(t,sqrt,'r--', label=r'$\sigma = \sqrt{t}$') #plots standard deviation of exact solution values, red dashed line
    plt.plot(t,msqrt,'r--') #plots standard deviation of exact solution values, red dashed line
    plt.legend(loc='best') #produces a legend

    
    
#---------------------
def wienerM(dt,X0,Nt,M):
    """ Input variables:
    dt    time step
    X0    intial value, X(t=0) = X0
    Nt    number of time steps 
    M     number of samples
    """
    Xdt=np.sqrt(dt)*npr.randn(M,Nt+1) #creates a M x Nt+1 matrix of random numbers multipled by square root of dt which will be our X values
    Xdt[:,0]=X0 #makes the first column of our Xdt matrix the starting value of X, X0
    w=np.cumsum(Xdt,axis=1) #cumulatively sums each entry of our Xdt row, which represents the weiner procresss with timestep, dt, and initial condition X0
    me=np.mean(w,axis=0) #creates an array of the mean for X(t) from our sample, size M
    std=np.std(w,axis=0) #creates an array of the standard deviation for X(t) from our sample, size M
    t=np.linspace(0,dt*Nt,Nt+1)
    mu=np.linspace(X0,X0,Nt+1) #creates an array of the analytical mean, to be plotted against time
    sqrt=np.sqrt(t) #creates an array of the analytical standard deviation, to be plotted against time
    msqrt=(-1)*np.sqrt(t)
    plt.figure()
    plt.xlabel('t')
    plt.ylabel('X(t)')
    plt.title('wienerM, Wale Osikomaiya, Monte Carlo simulation of Weiner process with $M$=%d , $dt$=%2.3f , $X_{0}$=%d , $Nt$=%d' %(M, dt, X0, Nt))
    plt.plot(t,me,'b-', label='computed $\mu$') #plots computed mean against time, blue line
    plt.plot(t,mu,'b--', label=r'$\mu = X_{0}$') #plots analytical mean against time, blue dashed line
    plt.plot(t,std,'g-', label='computed $\sigma$') #plots computed standard deviation against time, green line
    plt.plot(t,sqrt,'r--', label=r'$\sigma = \sqrt{t}$') #plots analytical standard deviation against time, red dashed line
    plt.plot(t,msqrt,'r--') #plots analytical standard deviation against time, red dashed line
    plt.legend(loc='best')


#-----------------------------
def langevin1(dt,V0,Nt,g=1.0):
    """ Input variables:
    dt    time step
    V0    intial value, V(t=0) = V0
    Nt    number of time steps 
    g     friction factor, gamma
    """
    Vdt=np.zeros(Nt+1) #creates an array of Nt+1
    Vdt[0]=V0 #makes the first entry of our Vdt array the starting value of V, V0
    randn=npr.randn(Nt) #creates an array of Nt+1 of random numbers
    for i in range(1, Nt+1): #for loop which applies the langevin equation using the pervious entry and an entry from our random matrix 
        Vdt[i]=Vdt[i-1]-(g*Vdt[i-1]*dt)+(np.sqrt(dt)*randn[i-1]) #then enters the result in Vdt array
    t=np.linspace(0,dt*Nt,Nt+1)
    plt.figure()
    plt.xlabel('t')
    plt.ylabel('V(t)')
    plt.title('langevin1, Wale Osikomaiya, Langevin version of Weiner process with $dt$=%2.3f, $V_{0}$=%d, $Nt$=%d, g=%d ' %(dt, V0, Nt, g))
    plt.plot(t,Vdt,'b-') #plots values of V(t) against t, blue line
    
#-----------------------------
def langevinM(dt,V0,Nt,M,g=1.0,debug=False):
    """ Input variables:
    dt    time step
    V0    intial value, V(t=0) = V0
    Nt    number of time steps 
    M     number of samples
    g     friction factor, gamma
    debug flag to control plotting of figures
    """
    Vdt=np.zeros((M,Nt+1)) #creates an M x Nt+1 matrix
    dummy=np.zeros((M,Nt+1)) #creats our 'dummy' M x Nt+1 matrix which we will use to do the langevin equation calculations
    Vdt[:,0]=V0 #makes the first column of our Vdt matrix the starting value of V, V0
    for i in range(1, Nt+1): #for loop which applies the langevin equation using the columns
        dummy[:,i]=npr.randn(1,M) #the 'dummy' matrix has its column hold a random number for each sample
        Vdt[:,i]=Vdt[:,i-1]-(g*Vdt[:,i-1]*dt)+(np.sqrt(dt)*dummy[:,i]) #the langevin equation is applied for each element of our sample using the pervious V(t) value

    t=np.linspace(0,dt*Nt,Nt+1)
    meanVdt=np.mean(Vdt,axis=0) #creates an array of the mean for V(t) from our sample, to be plotted against time
    varVdt=(np.std(Vdt,axis=0))**2 #creates an  array of the standard deviation for V(t) from our sample then squares the values to get the variance, to be plotted against time
    mean_ana=np.zeros(Nt+1) #for loop to create an array of the analytical mean, to be plotted against time
    for  i in range(0, Nt+1):
        mean_ana[i]=V0*np.exp(-g*t[i])

    var_ana=np.zeros(Nt+1) #for loop to create an array of the analytical variance, to be plotted against time
    for  i in range(0, Nt+1):
        var_ana[i]=(1./float((2*g)))*(1-np.exp(-2*g*t[i]))
    epsi=np.abs(meanVdt-mean_ana) #step 1 to calculating the error
    epsil=((1./float(Nt))*np.cumsum(epsi)) #step 2 to calculating the error, summing
    epsilon=epsil[Nt] #step 3 to calculating the error, makes epsilon the error which is the final value of the cumulative sum

    if debug == True: #boolean statement so that when debug make True in our funtion we also get a plot as well as the error returned
        plt.figure()
        plt.xlabel('t')
        plt.ylabel('V(t)')
        plt.title('langevinM, Wale Osikomaiya, Langevin version of Monte Carlo simulation with $M$=%d, $dt$=%2.3f, $V_{0}$=%d, $Nt$=%d, g=%d ' %(M, dt, V0, Nt, g))
        plt.plot(t,meanVdt,'b-', label='computed $\mu$') # computed mean, blue line
        plt.plot(t,mean_ana,'b--', label=r'$\mu = V_{0}e^{-\gamma t}$') #analytical mean, blue dashed line
        plt.plot(t,varVdt,'g-', label='computed variance $\sigma^{2}$') # computed variance, green line
        plt.plot(t,var_ana,'g--', label=r'$\sigma^{2} =\frac{1}{2\gamma}(1-e^{-2\gamma t})$') #analytical variance, red dashed line
        plt.legend(loc='best')
        return epsilon
    else:
        return epsilon

#--------------------------------
def langevinTest(dt,V0,Nt,g=1.0):
    """ Input variables:
    dt    time step
    V0    intial value, V(t=0) = V0
    Nt    number of time steps 
    g     friction factor, gamma
    """
    M=10000
    error=np.zeros(4) #creates an array to hold our four errors 
    error[0]=langevinM(dt, V0, Nt, M/1000, g, False) #error 1 from langevinM when M=10
    error[1]=langevinM(dt, V0, Nt, M/100, g, False) #error 2 from langevinM when M=100
    error[2]=langevinM(dt, V0, Nt, M/10, g, False) #error 3 from langevinM when M=1000
    error[3]=langevinM(dt, V0, Nt, M/1, g, False) #error 4 from langevinM when M=10000
    plot_M=[10, 100, 1000, 10000] #values of M to be plotted against the error
    plt.figure()
    plt.xlabel('M value')
    plt.ylabel('error')
    plt.title('langevinTest, Wale Osikomaiya, Error of langevinM with $dt$=%2.3f, $V_{0}$=%d, $Nt$=%d, g=%d ' %(dt, V0, Nt, g))
    plt.loglog(plot_M,error) #plots a log-log plot of M against the error produce by langevinM
    y=np.log(error)
    x=np.log(plot_M)
    z=np.polyfit(x,y,1) #computes a linear least-squares fit for log(M) vs log(error) to obtain an estimate for n in M^{−n}
    return np.abs(z[0]) #returns our value for n, where we've inferred error behaves as M^{−n}
#---------------------------------
#The section below is included for assessment and *must* be included exactly as below in the final
#file that you submit, but you may want to omit it (e.g. comment it out) while you are developing
#your code.  
if __name__ == "__main__":
    npr.seed(1)
    test_randn(1000)
    wiener1(0.1,0.0,1000)
    wienerM(0.1,2.0,1000,20000)
    langevin1(0.1,0.0,1000,0.01)
    print "Error from LangevinM, dt=%1.2f,V0=%1.2f,Nt=%d,M=%d,g=%1.3f: "%(0.1,1.0,1000,2000,0.05), langevinM(0.1,1.0,1000,2000,0.05,True)
    print "Convergence rate for Langevin Monte Carlo calculations from langevinTest", langevinTest(0.1,1.0,1000,0.25)
    plt.show()