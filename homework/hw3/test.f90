program test
    !For this code to work, the user must:
    !1. add variable declarations, alpha (double prec),
    !and error (array of double prec.)
    !2. tell the code where it can find information on dx, N, and test_fd
    use fdmodule
    implicit none
    integer :: i3
    real(kind=8), dimension(:), allocatable :: x
    double precision :: x0
    double precision :: alpha
    real(kind=8), dimension(:), allocatable :: f1
    real(kind=8), dimension(:), allocatable :: df1

    open(unit=10,file='data.in')
        read(10,*) n
        read(10,*) alpha
    close(10)

    allocate(f1(n))
    allocate(df1(n))
    allocate(x(n))

    dx = 2.d0/dble(n-1)

    do i3=1,n
        x(i3)=((i3-1)*dx)
    end do

    x0=0.5*maxval(x)

    f1 = sin(x)
    call fd2(f1, df1)
    print *, 'df1=',df1
end program test