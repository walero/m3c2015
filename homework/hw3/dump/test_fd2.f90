program test_fd2
	implicit none
    integer, parameter :: n =10
    double precision :: dx = 0.1
    double precision, parameter :: x0 = 3
    integer :: i1,i2
    real(kind=8), dimension(n) :: x
    real(kind=8), dimension(n) :: f
    real(kind=8), dimension(size(f)) :: df

    x(1)=x0
    do i1=1,n
    	x(i1)=x(1)+((i1-1)*dx)
    end do

    f=sin(x)

    df(1)=(f(2)-f(n))/(2*dx)
    do i2=2,n-1
    	df(i2)=(f(i2+1)-f(i2-1))/(2*dx)
    end do
    df(n)=(f(1)-f(n-1))/(2*dx)
    print *, 'f =',f
    print *, 'df = ',df

end program test_fd2
