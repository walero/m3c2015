program testfd
	implicit none
    integer :: i3
    real(kind=8) :: alpha = 1
    real(kind=8) :: error(2)
    integer, parameter :: n = 20
    double precision :: dx = 0.1
    real(kind=8), dimension(n) :: x
    double precision :: x0
    real(kind=8), dimension(n) :: f_g
    real(kind=8), dimension(size(f_g)) :: df_g
    real(kind=8), dimension(size(f_g)) :: df_2

    save

    do i3=1,n
    	x(i3)=((i3-1)*dx)
    end do

    x0=0.5*maxval(x)
    f_g=exp(-alpha*(x-x0)**2)
    df_g=-2*alpha*(x-x0)*f_g

    call test_fd2(f_g, df_2)

    error(1)=(1/n)*sum(abs(df_g-df_2))

    print *, 'L1 error =',error(1)
    print *, 'error =',error
    print *, 'wtf',df_2
end	program testfd
!-----------------------------------------
subroutine test_fd2(f,df)
	implicit none
    double precision :: dx
    double precision :: x0
    integer :: n
    integer :: i1,i2
    real(kind=8), dimension(n) :: x_dum
    real(kind=8), dimension(n), intent(in) :: f
    real(kind=8), dimension(size(f)), intent(out) :: df

    !allocate(f(n))
    !allocate(x_dum(n))
    x_dum(1)=x0
    do i1=1,n
    	x_dum(i1)=x_dum(1)+((i1-1)*dx)
    end do

    df(1)=(f(2)-f(n))/(2*dx)
    do i2=2,n-1
    	df(i2)=(f(i2+1)-f(i2-1))/(2*dx)
    end do
    df(n)=(f(1)-f(n-1))/(2*dx)
end subroutine test_fd2