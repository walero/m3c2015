!Wale Osikomaiya CID: 00734031
!Module containing routines for differentiating array of size N with
!2nd order and 4th order compact finite differences
!Test functions apply these methods to a Gaussian function and
!return performance information.

module fdmodule
    implicit none
    integer :: n !n grid points 
    double precision :: dx !dx grid spacing
    save

contains
!-------------------
subroutine fd2(f,df)
    !2nd order centered finite difference
    !Header variables
    !f: function to be differentiated
    !df: derivative of f
    implicit none
    integer :: i2
    real(kind=8), dimension(:), intent(in) :: f
    real(kind=8), dimension(size(f)), intent(out) :: df

    df(1)=(f(2)-f(n))/(2*dx)
    do i2=2,n-1
        df(i2)=(f(i2+1)-f(i2-1))/(2*dx)
    end do

    df(n)=(f(1)-f(n-1))/(2*dx)
end subroutine fd2
!-----------------

subroutine cfd4(f,df)
    !compact 4th order centered finite difference
    !Header variables
    !f: function to be differentiated
    !df: derivative of f
    implicit none
    real(kind=8), dimension(:), intent(in) :: f
    real(kind=8), dimension(size(f)), intent(out) :: df
    integer :: NRHS, LDB, INFO, i3, i4, i5, i6
    double precision, dimension(:,:), allocatable :: B, Btemp
    double precision, dimension(n) :: D
    double precision, dimension(n-1) :: DL, DU

    NRHS=1
    LDB=n
    allocate(B(LDB, NRHS), Btemp(LDB, NRHS))

    D(1)=1
    do i3=2,n-1
        D(i3)=4
    end do
    D(n)=1

    DU(1)=1
    do i4=2,n
        DU(i4)=1
    end do
    
    do i5=1,n-1
        DL(i5)=1
    end do
    DL(1)=2

    Btemp(1,1)=-((5.d0/2)*f(1))+(2*f(2))+((1.d0/2)*f(3))
    Btemp(n,1)=((5.d0/2)*f(n))-(2*f(n-1))-((1.d0/2)*f(n-2))
    do i6=2,n-1
        Btemp(i6,1)=3*(f(i6+1)-f(i6-1))
    end do
    B=Btemp*(1.d0/dx)

    call  dgtsv(n, NRHS, DL, D, DU, B, LDB, INFO) !solve cfd Ax=B
    df=B(:,1)
    !print *, 'INFO=', INFO
end subroutine cfd4
!------------------

subroutine test_fd(alpha,error)
    !test accuracy of fd2 and cfd4 with a Gaussian function, f=exp(-alpha*(x-x0)**2)
    !Header variables:
    !alpha: sets width of Gaussian
    !error: 2-element array containing fd2 and cfd4 approximation errors
    implicit none
    integer :: i1
    real(kind=8), intent(in) :: alpha
    real(kind=8), intent(out) :: error(2)
    real(kind=8), dimension(:), allocatable :: x
    double precision :: x0
    real(kind=8), dimension(:), allocatable :: f_g
    real(kind=8), dimension(:), allocatable :: df_g
    real(kind=8), dimension(:), allocatable :: df2, df4

    allocate(x(n))
    allocate(f_g(n))
    allocate(df_g(n))
    allocate(df2(n), df4(n))

    do i1=1,n
        x(i1)=((i1-1)*dx)
    end do

    x0=0.5*maxval(x)
    f_g=exp(-alpha*(x-x0)**2)
    df_g=-2*alpha*(x-x0)*f_g

    call fd2(f_g, df2)
    call cfd4(f_g, df4)

    error(1)=(1.d0/n)*sum(abs(df_g-df2))
    error(2)=(1.d0/n)*sum(abs(df_g-df4))

    !print*,'L1norm error fd2 =', error(1)
    !print*,'L1norm error cfd4 =', error(2)

end subroutine test_fd
!---------------------

subroutine test_fd_time(alpha,error,time)
    !test accuracy and speed of fd2 and cfd4 with a Gaussian function, f=exp(-alpha*(x-x0)**2)
    !Header variables:
    !alpha: sets width of Gaussian
    !error: 2-element array containing fd2 and cfd4 approximation errors
    !time: 2-element array containing wall-clock time required by 1000 calls
    !to fd2 and cfd4
    implicit none
    real(kind=8), intent(in) :: alpha
    real(kind=8), intent(out) :: error(2),time(2)
    integer :: i7, i8, i9
    real(kind=8), dimension(:), allocatable :: x
    double precision :: x0
    real(kind=8), dimension(:), allocatable :: f_g
    real(kind=8), dimension(:), allocatable :: df_g
    real(kind=8), dimension(:), allocatable :: df2, df4
    !real(kind=8) :: cpu_t1,cpu_t2,clock_time
    integer(kind=8) :: tock1, tock2, tick1, tick2, rate1, rate2

    allocate(x(n))
    allocate(f_g(n))
    allocate(df_g(n))
    allocate(df2(n), df4(n))

    do i7=1,n
        x(i7)=((i7-1)*dx)
    end do

    x0=0.5*maxval(x)
    f_g=exp(-alpha*(x-x0)**2)
    df_g=-2*alpha*(x-x0)*f_g

    call system_clock(tick1)
    do i8=1,1000
        call fd2(f_g, df2)
    end do
    call system_clock(tick2,rate1)
    time(1)=dble(tick2-tick1)/dble(rate1)

    call system_clock(tock1)
    do i9=1,1000
        call cfd4(f_g, df4)
    end do
    call system_clock(tock2,rate2)
    time(2)=(dble(tock2-tock1)/dble(rate2))!-time(1)
    
    error(1)=(1.d0/n)*sum(abs(df_g-df2))
    error(2)=(1.d0/n)*sum(abs(df_g-df4))

    !print*,'L1norm error fd2 =', error(1)
    !print*,'L1norm error cfd4 =', error(2)
    !print*, 'wall time elapsed fd2 =',time(1)
    !print*, 'wall time elapsed cfd4 =',time(2)


end subroutine test_fd_time
!---------------------------
end module fdmodule















