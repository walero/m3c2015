program test
    !For this code to work, the user must:
    !1. add variable declarations, alpha (double prec),
    !and error (array of double prec.)
    !2. tell the code where it can find information on dx, N, and test_fd
    use fdmodule
    implicit none
    real(kind=8) :: L1epsilon(2)
    double precision :: alpha

    open(unit=10,file='data.in')
        read(10,*) n
        read(10,*) alpha
    close(10)

    dx = 2.d0/dble(N-1)

    call test_fd(alpha, L1epsilon)
end program test